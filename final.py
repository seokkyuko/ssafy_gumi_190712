# -*- coding: utf-8 -*-
import re
import urllib.request

from bs4 import BeautifulSoup

from flask import Flask, request
from slack import WebClient
from slackeventsapi import SlackEventAdapter
import pickle
import json
import time

file1 = open('lunchbox', 'rb')
lunchbox = pickle.load(file1)
#print(lunchbox)
file1.close()
file2 = open('rice', 'rb')
rice = pickle.load(file2)
#print(rice)
file2.close()
file3 = open('bread', 'rb')
bread = pickle.load(file3)
#print(bread)
file3.close()
file4 = open('beverage', 'rb')
beverage = pickle.load(file4)
#print(beverage)
file4.close()

SLACK_TOKEN = "xoxb-672063103650-678238132226-zKQEGv1oEHN0Icf5XmcEyqX9"
SLACK_SIGNING_SECRET = "e85f8e201cba979f6231624ef0734e6d"

app = Flask(__name__)
# /listening 으로 슬랙 이벤트를 받습니다.
slack_events_adaptor = SlackEventAdapter(SLACK_SIGNING_SECRET, "/listening", app)
slack_web_client = WebClient(token=SLACK_TOKEN)

price_sum = 0
sel_list = ""

# your attachment
attachments_json  =  [
    {
        "fallback": "먹을 것을 선택할 수 없습니다",
        "callback_id": "first",
        "color": "#3AA3E3",
        "attachment_type": "default",
        "actions": [
            {
                "name": "1",
                "text": "도시락",
                "type": "button",
                "value": "1"
            },
            {
                "name": "2",
                "text": "김밥",
                "type": "button",
                "value": "2"
            },
            {
                "name": "3",
                "text": "햄버거/샌드위치",
                "type": "button",
                "value": "3",
            }
        ]
    }
]

def _crawl_store(text):
    text = text.split('> ')[1]
    print(text)
    if ('7' in text) | ('seven' in text):
        return 1
    else:
        return 0


# 챗봇이 멘션을 받았을 경우
@slack_events_adaptor.on("app_mention")
def app_mentioned(event_data):
    global price_sum
    global sel_list
    price_sum = 0
    sel_list = "----- 고른 물품 ------\n"


    channel = event_data["event"]["channel"]
    text = event_data["event"]["text"]
    keywords = _crawl_store(text)
    # slack_web_client.chat_postMessage(
    #     channel=channel,
    #     text=keywords
    # )
    if keywords == 1:
        slack_web_client.chat_postMessage(
            channel=channel,
            text="Hello User"
        )
        slack_web_client.chat_postMessage(
            channel=channel,
            text="식사 메뉴를 선택을 해주세요",
            attachments = attachments_json
        )
    else:
        slack_web_client.chat_postMessage(
            channel=channel,
            text="잘못된 입력입니다."
        )
        exit(1)

@app.route("/click", methods=["POST"])
def on_button_click():
    global price_sum
    global sel_list
    data = json.loads(request.form['payload'])
    #print(data)
    data_ex = data['actions'][0]
    if data_ex['name'] == '1':
        attachments_json = [
        {
            "fallback": "If you could read this message, you'd be choosing something fun to do right now.",
            "color": "#3AA3E3",
            "attachment_type": "default",
            "callback_id": "lunchbox",
            "actions": [
                {
                    "name": "lunchbox",
                    "text": "lunchbox 음식을 선택하세요",
                    "type": "select"
                }
            ]
        }
    ]
        actions = []
        i = 0
        for menu, price in lunchbox.items():
            temp = {}
            temp["text"] = menu+"\t"+str(price)+"원"
            temp["value"] = "lunchbox "+str(i)
            i = i+1
            actions.append(temp)
        attachments_json[0]["actions"][0]["options"] = actions

        print(attachments_json)

    elif data_ex['name'] == '2':
        attachments_json = [
            {
                "fallback": "If you could read this message, you'd be choosing something fun to do right now.",
                "color": "#3AA3E3",
                "attachment_type": "default",
                "callback_id": "rice",
                "actions": [
                    {
                        "name": "rice",
                        "text": "rice 음식을 선택하세요",
                        "type": "select"
                    }
                ]
            }
        ]
        actions = []
        i = 0
        for menu, price in rice.items():
            temp = {}
            temp["text"] = menu+"\t"+str(price)+"원"
            temp["value"] = "rice " + str(i)
            i = i + 1
            actions.append(temp)
        attachments_json[0]["actions"][0]["options"] = actions

        print(attachments_json)
    elif data_ex['name'] == '3':
        attachments_json = [
            {
                "fallback": "If you could read this message, you'd be choosing something fun to do right now.",
                "color": "#3AA3E3",
                "attachment_type": "default",
                "callback_id": "bread",
                "actions": [
                    {
                        "name": "bread",
                        "text": "bread 음식을 선택하세요",
                        "type": "select"
                    }
                ]
            }
        ]
        actions = []
        i = 0
        for menu, price in bread.items():
            temp = {}
            temp["text"] = menu+"\t"+str(price)+"원"
            temp["value"] = "bread " + str(i)
            i = i + 1
            actions.append(temp)
        attachments_json[0]["actions"][0]["options"] = actions

        print(attachments_json)
    else:
        if data_ex['name'] == 'lunchbox':
            #data_ex['selected_options'][0]['value'].split()[1]
            attachments_json = [
                {
                    "fallback": "If you could read this message, you'd be choosing something fun to do right now.",
                    "color": "#3AA3E3",
                    "attachment_type": "default",
                    "callback_id": "beverage",
                    "actions": [
                        {
                            "name": "beverage",
                            "text": "beverage 음식을 선택하세요",
                            "type": "select"
                        }
                    ]
                }
            ]
            actions = []
            i = 0
            for menu, price in beverage.items():
                temp = {}
                temp["text"] = menu+"\t"+str(price)+"원"
                temp["value"] = "beverage " + str(i)
                i = i + 1
                actions.append(temp)
            attachments_json[0]["actions"][0]["options"] = actions

            num = int(data_ex['selected_options'][0]['value'].split()[1])
            price = list(lunchbox.values())[num]

            selected = list(lunchbox.keys())[num]
            sel_list += selected+" : "+str(price) +"원\n"

            #global price_sum
            price_sum = price_sum + price

        elif data_ex['name'] == 'rice':
            attachments_json = [
                {
                    "fallback": "If you could read this message, you'd be choosing something fun to do right now.",
                    "color": "#3AA3E3",
                    "attachment_type": "default",
                    "callback_id": "beverage",
                    "actions": [
                        {
                            "name": "beverage",
                            "text": "beverage 음식을 선택하세요",
                            "type": "select"
                        }
                    ]
                }
            ]
            actions = []
            i = 0
            for menu, price in beverage.items():
                temp = {}
                temp["text"] = menu+"\t"+str(price)+"원"
                temp["value"] = "beverage " + str(i)
                i = i + 1
                actions.append(temp)
            attachments_json[0]["actions"][0]["options"] = actions

            num = int(data_ex['selected_options'][0]['value'].split()[1])
            price = list(rice.values())[num]

            selected = list(rice.keys())[num]
            sel_list += selected+" : "+str(price) +"원\n"

            #global price_sum
            price_sum += price

        elif data_ex['name'] == 'bread':
            attachments_json = [
                {
                    "fallback": "If you could read this message, you'd be choosing something fun to do right now.",
                    "color": "#3AA3E3",
                    "attachment_type": "default",
                    "callback_id": "beverage",
                    "actions": [
                        {
                            "name": "beverage",
                            "text": "beverage 음식을 선택하세요",
                            "type": "select"
                        }
                    ]
                }
            ]
            actions = []
            i = 0
            for menu, price in beverage.items():
                temp = {}
                temp["text"] = menu+"\t"+str(price)+"원"
                temp["value"] = "beverage " + str(i)
                i = i + 1
                actions.append(temp)
            attachments_json[0]["actions"][0]["options"] = actions

            num = int(data_ex['selected_options'][0]['value'].split()[1])
            price = list(bread.values())[num]

            selected = list(bread.keys())[num]
            sel_list += selected+" : "+str(price) +"원\n"

            #global price_sum
            price_sum += price

        else:
            #data_ex['selected_options'][0]['value'].split()[1]
            num = int(data_ex['selected_options'][0]['value'].split()[1])
            price = list(beverage.values())[num]

            selected = list(beverage.keys())[num]

            sel_list += selected+" : "+str(price) +"원\n"
            #global price_sum
            price_sum += price

            slack_web_client.chat_postMessage(
                channel=data['channel']['id'],
                text= sel_list+"------------------------\n최종 가격은 " + str(price_sum) + "원 입니다.",
            )

            time.sleep(1)
            exit(1)

    slack_web_client.chat_postMessage(
        channel=data['channel']['id'],
        text="메뉴 선택을 해주세요.\n"+sel_list+"------------------------\n현재 총 가격은 "+str(price_sum)+" 입니다.",
        attachments = attachments_json
    )

# / 로 접속하면 서버가 준비되었다고 알려줍니다.
@app.route("/", methods=["GET"])
def index():

    return "<h1>Server is ready.</h1>"


if __name__ == '__main__':
    app.run('127.0.0.1', port=4040)
